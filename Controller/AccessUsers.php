<?php
require ('../Model/Conexion.php');
require ('Constans.php');

//si no existe la sesion, iniciamos una
if (!isset($_SESSION)) {
    session_start();
}

//el parametro que enviememos del formulario lo colocamos en la variable
$usuario = $_GET['usuario'];
$password = $_GET['password'];

// ya con los datos se conecta a la BD
$con = new Conexion();


//busca y verifica el usuario
// para eso va a una consulta qie esta en el archivo de conexion
//getUser es el nombre de la funcion a la que va
$searchUser = $con->getUser($usuario,$password);


//colocamos los datos de los usuarios en variables
foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}
$colorElegido="#4e4e4e";
$colorDefecto="#0061c2";
$idMenu="1";

$updateMenuColorElegido=$con->updateOpcionElegida($colorElegido,$idMenu);
$updateMenuColorDefecto=$con->updateOpcionDefecto($colorDefecto,$idMenu);


//1) Si el usuario no existe le colocamos alerta
if (empty($searchUser)) {
    echo '
     <script language = javascript>
	alert("Usuario o Password incorrectos, por favor intenta de nuevo")
	self.location = "../index.php"
	</script>
    ';
}
else if ($tipo == 'VENTAS'){

    $urlViews = URL_VIEWS;
    $userLogueado = $nombres;
    $imageUser =$foto;
    $menuMain = $con->getMenuMainVentas();

    require ('../Views/WellcomeVentas.php');

}
else if ($tipo == 'ADMINISTRADOR'){

     $urlViews = URL_VIEWS;
     $userLogueado = $nombres;
     $imageUser =$foto;


      $menuMain = $con->getMenuMain();
      
      require ('../Views/Wellcome.php');
}

?>